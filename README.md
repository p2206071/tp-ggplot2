# **TP Visualisation de données**
## Visualisation de résultats de simulations de dynamique moléculaire avec ggplot2
*Cornélius Venturini*

## I/ Code R

```
install.packages("ggplot2")
library(ggplot2)
D_inter=read.table("Desktop/Distances_inter.txt")

ggplot(D_inter, aes(Time)) +
  geom_line(aes(Time, d1A_6B, color ="d1A_6B"), alpha=0.5) +
  geom_smooth(aes(Time, d1A_6B, color ="d1A_6B")) +
  geom_line(aes(Time, d6A_1B, color ="d6A_1B"), alpha=0.5) +
  geom_smooth(aes(Time, d6A_1B, color="d6A_1B")) +
  geom_line(aes(Time, d6A_6B, color ="d6A_6B"), alpha=0.5) +
  geom_smooth(aes(Time, d6A_6B, color ="d6A_6B")) +
  geom_line(aes(Time, d1A_1B, color ="d1A_1B"), alpha=0.5) +
  geom_smooth(aes(Time, d1A_1B, color ="d1A_1B")) +
  facet_wrap(~run) +
  ggtitle("Évolution de la distance entre 2 points de la protéine en fonction du temps de 4 simulations") +
  labs(x = 'Temps', y = 'Distance') +
  scale_color_manual(values=c(d1A_6B="darkorange1", d6A_1B = "darkorange3", d6A_6B="blue4", d1A_1B="blue1"), labs(color="distance"))
```

## II/ graphique

![](./graph.png)

## III/ Interprétation

Nous nous intéressons ici à une protéine membranaire. Une simulation de 500 ns à été réalisé sur ce dimère, et les distances séparant différentes hélices alpha ont été mesuré au cours du temps. Le repliement de la protéine laisse apparaître un site creux.

Nous pouvons ici observer l’évolution des distances entre deux points placé du même coté de ce site (en orange) et entre deux points de part et d’autre de ce site (en bleu).

On voit que les distance en bleu évolue de la même façon quelque soit la simulation. Tandis que les distances orange, montrent plus de variation. On se serait attendu à l’inverse car entre les points 1A et 6B et entre 1B et 6A, la structure de la protéine ne montre rien de particulier par rapport au point 1A-1B et 6A-6B

De plus la courbe bleu claire montre que les points 1A et 1B sont aussi proche, que les points du même coté du site d’intérêt alors que les points 6A et 6B sont bien plus éloigné. On peut donc pensé que la région creuse est plus forme de cerf-volant que de carré.
