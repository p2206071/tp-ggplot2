install.packages("ggplot2")

library(ggplot2)
D_inter=read.table("Desktop/Distances_inter.txt")

ggplot(D_inter, aes(Time)) +
  geom_line(aes(Time, d1A_6B, color ="d1A_6B"), alpha=0.5) +
  geom_smooth(aes(Time, d1A_6B, color ="d1A_6B")) +
  geom_line(aes(Time, d6A_1B, color ="d6A_1B"), alpha=0.5) +
  geom_smooth(aes(Time, d6A_1B, color="d6A_1B")) +
  geom_line(aes(Time, d6A_6B, color ="d6A_6B"), alpha=0.5) +
  geom_smooth(aes(Time, d6A_6B, color ="d6A_6B")) +
  geom_line(aes(Time, d1A_1B, color ="d1A_1B"), alpha=0.5) +
  geom_smooth(aes(Time, d1A_1B, color ="d1A_1B")) +
  facet_wrap(~run) +
  ggtitle("Évolution de la distance entre 2 points de la protéine en fonction du temps de 4 simulations") +
  labs(x = 'Temps', y = 'Distance') +
  scale_color_manual(values=c(d1A_6B="darkorange1", d6A_1B = "darkorange3", d6A_6B="blue4", d1A_1B="blue1"), labs(color="distance"))
